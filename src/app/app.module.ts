import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { RegisterFormComponent } from './register/register-form/register-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from './app-routing.module';
import { SystemRoutingModule } from './system-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MyNavComponent } from './my-nav/my-nav.component';

import { TeacherActivityListComponent } from './teacher/teacher-activity-list/teacher-activity-list.component';

import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';

import { StudentactivityComponent } from './teacher/studentactivity/studentactivity.component';
import { AdminActivityListComponent } from './admin/activity-list/activity-list.component';
import { AddActivityComponent } from './admin/add-activity/add-activity.component';
import { AdminStudentTableComponent } from './admin/admin-student-table/admin-student-table.component';

import { UpdateStudentComponent } from './students/update-student/update-student/update-student.component';
import { StudentService } from './service/student-service';
import { StudentServiceImpl } from './service/student-service-impl.service';
import { MatDatepickerModule, MatNativeDateModule, MatSortModule, MatTableModule, MatPaginatorModule } from '@angular/material';
import { StudentEnrolledListComponent } from './students/enrolled-activity-list/student-enrolled-list/student-enrolled-list.component';
import { ActivityService } from './service/activity-service';
import { ActivityServiceImpl } from './service/activity-service-impl';
import { ViewActivityListComponent } from './students/view-activity-list/view-activity-list.component';
import { TeacherService } from './service/teacher-service.service';
import { TeacherServiceImpl } from './service/teacher-service-impl.service';
import { StudentUpdateModalComponent } from './students/student-update-modal/student-update-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    RegisterFormComponent,
    MyNavComponent,
    TeacherActivityListComponent,
    EditActivityComponent,
    StudentactivityComponent,
    AdminActivityListComponent,
    AddActivityComponent,
    AdminStudentTableComponent,
    UpdateStudentComponent,
    ViewActivityListComponent,
    StudentUpdateModalComponent,
    StudentEnrolledListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
  AngularMaterialModule,
  HttpClientModule,
  MatDatepickerModule,
    MatNativeDateModule ,
  ReactiveFormsModule,
  FormsModule,
    AngularMaterialModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    FormsModule,
    AngularMaterialModule,
    FlexLayoutModule,
    SystemRoutingModule,
    AppRoutingModule,
    MatSortModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [
    { provide: StudentService, useClass: StudentServiceImpl},
    { provide: ActivityService, useClass: ActivityServiceImpl},
    { provide: TeacherService, useClass: TeacherServiceImpl},
  
    
   
  ],
  bootstrap: [AppComponent],
  entryComponents: [StudentUpdateModalComponent]
})
export class AppModule { }
