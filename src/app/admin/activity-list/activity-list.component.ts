import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import {  AdminActivityListDataSource} from './activity-list-datasource';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { TeacherService } from 'src/app/service/teacher-service.service';

@Component({
  selector: 'app-activity-list',
  templateUrl: './activity-list.component.html',
  styleUrls: ['./activity-list.component.css']
})
export class AdminActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: AdminActivityListDataSource ;

/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = ['id', 'name', 'location','hostTeacher','regisPeriodStart','regisPeriodEnd','activityPeriodStart','activityPeriodEnd'];
activities: Activity[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private activityService: ActivityService,private teacherService: TeacherService) { }
  //Add data into the table
  addData() {
    this.activityService.getActivityList()
    .subscribe(activities =>  
      setTimeout(
        () => {
      this.dataSource = new AdminActivityListDataSource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.activities = activities;
      console.log(activities.length);
    }
    )
    ),0}
  ngOnInit() {
    
  }

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  
  
  convertDate(date: string) {return eval(date); }

  getHostTeacher(hostTeacherId: number) : string {
    var teacherN : string;
    this.teacherService.getTeacher(hostTeacherId).subscribe(
      (teacher) => {
        teacherN = teacher.name + " " + teacher.surname;
        console.log(teacherN);
        
      }
    )
    return teacherN;
  }
}
