import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Teacher from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-add-activity',
  templateUrl: './add-activity.component.html',
  styleUrls: ['./add-activity.component.css']
})
export class AddActivityComponent implements OnInit {
  constructor(private fb:FormBuilder,private router:Router,private activityService: ActivityService,private teacherService :TeacherService) {}
  form = this.fb.group({
    id: [''],
    name: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: ['',Validators.compose([Validators.required])],
    regisPeriodStart: [''],
    regisPeriodEnd: [''],
    activityPeriodStart: [''],
    activityPeriodEnd: [''],
    /*
    regisPeriodStartDate: [''],
    regisPeriodStartTime: [''],
    regisPeriodEndDate: [''],
    regisPeriodEndTime: [''],
    activityPeriodStart: [''],
    activityPeriodStartTime: [''],
    activityPeriodEndDate: [''],
    activityPeriodEndTime: [''],
    */
    hostTeacher: ['']
    
  })

  validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'description': [
      { type: 'required', message: 'Please enter the activity description'}
    ],
   
  };

  

  //Available teachers to host the activity
  hostTeachers : Teacher[];
  //Must convert form to actual object, just in case
  submit() {
    this.activityService.saveActivity(this.form.value).subscribe(
      (activity) => {
        alert('A new activity has been added!')
        
        this.router.navigate(['admin/activitylist']);
      }, (error)=> { alert('could not save value');
    })
  }

  ngOnInit() {
    //Add teachers
    this.teacherService.getTeachers().subscribe(
      (teachers) => {
        this.hostTeachers = teachers;
      }
    )
  }

  quickSetup() {
    this.form.patchValue({name:'Hello world'});
    this.form.patchValue({location:"CAMT"});
    this.form.patchValue({description:"No one read description"});
    //These will be fixed when we change Activity date
    this.form.patchValue({regisPeriodStart:new Date(2019,9,2).toISOString().split('T')[0]});
    this.form.patchValue({regisPeriodEnd:new Date(2019,9,15).toISOString().split('T')[0]});
    this.form.patchValue({activityPeriodStart:new Date(2019,10,1).toISOString().split('T')[0]});
    this.form.patchValue({activityPeriodEnd:new Date(2019,10,1).toISOString().split('T')[0]});
  }

}
