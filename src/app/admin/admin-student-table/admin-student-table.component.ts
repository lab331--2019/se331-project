import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { AdminStudentTableDataSource } from './admin-student-table-datasource';
import { BehaviorSubject, of } from 'rxjs';
import Student from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student-service';

@Component({
  selector: 'app-admin-student-table',
  templateUrl: './admin-student-table.component.html',
  styleUrls: ['./admin-student-table.component.css']
})
export class AdminStudentTableComponent implements AfterViewInit,OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Student>;
  dataSource: AdminStudentTableDataSource;

/** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
displayedColumns = ['id', 'studentId', 'name', 'surname','email','password','dob','image','accept', 'reject'];
students: Student[];
  filter: string;
  filter$: BehaviorSubject<string>;
  constructor(private studentService: StudentService) { }

  ngOnInit() {
   
  }
  addData() {
    this.studentService.getStudents()
    .subscribe(students => {
      setTimeout(
        () => {
      this.dataSource = new AdminStudentTableDataSource();
      this.dataSource.data = students;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.students = students;
      
      
    },0
    )
  })
 
}

  ngAfterViewInit() {
    this.addData();
  }
  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }
  
  convertDate(date: string) {return eval(date); }
  

  //Accept newly registered student
  accept(student:Student) {
    //Save to DB
    student.approved = true;
    this.studentService.saveStudent(student);
    this.addData();
  }
  //Reject newly registered student
  reject(student:Student) {
    if (confirm("Are you sure you want to reject this student's registration?")) {
     //Delete from DB
      this.studentService.deleteStudent(student.id); 
    {
      this.studentService.getStudents().subscribe(
        (result) => console.log(result.length)
      );
      this.addData();
    }
    }
   
    //Refresh page
  }


}