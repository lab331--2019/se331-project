import Student from './student';
import Teacher from './teacher';
export default class Activity {
    id: number;
    name: string;
    location: string;
    description: string;
    hostTeacher: number; //Refer to teacher by id
    regisPeriodStart: Date;
    regisPeriodEnd: Date
    activityPeriodStart: Date;
    activityPeriodEnd: Date;
    waitingStudents: number[];  //Refer to each student by id
    enrolledStudents: number[]; //Refer to each student by id




}
