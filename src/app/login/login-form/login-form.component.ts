import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  form = this.fb.group({
   
    email: [''],
    password: ['']
  
  })
  constructor(private fb: FormBuilder,private router:Router) { }
submit() {
  if (this.form.get('email').value == '' || this.form.get('password').value == '') {
    alert("Please reinput your email and password!");
  }
  else {
    alert("You're logged in!");
    this.router.navigate(['student/viewlist']);
    
  }
}
  ngOnInit() {
  }
  

}
