import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginFormComponent } from './login/login-form/login-form.component';
import { RegisterFormComponent } from './register/register-form/register-form.component';
import { TeacherActivityListComponent } from './teacher/teacher-activity-list/teacher-activity-list.component';
import { EditActivityComponent } from './teacher/edit-activity/edit-activity.component';
import { StudentactivityComponent } from './teacher/studentactivity/studentactivity.component';
import { AdminActivityListComponent } from './admin/activity-list/activity-list.component';
import { AddActivityComponent } from './admin/add-activity/add-activity.component';
import { AdminStudentTableComponent } from './admin/admin-student-table/admin-student-table.component';
import { UpdateStudentComponent } from './students/update-student/update-student/update-student.component';
import { StudentEnrolledListComponent } from './students/enrolled-activity-list/student-enrolled-list/student-enrolled-list.component';
import { ViewActivityListComponent } from './students/view-activity-list/view-activity-list.component';


const systemRoutes: Routes = [
  { path: 'login', component: LoginFormComponent},
  { path: 'register',component: RegisterFormComponent},
  { path: 'teacher/activitylist', component: TeacherActivityListComponent},
  { path: 'teacher/editactivity/:id', component: EditActivityComponent },
  { path: 'teacher/studentactivity/:id', component: StudentactivityComponent},
  { path: 'admin/activitylist',component: AdminActivityListComponent},
  { path: 'admin/addactivity',component: AddActivityComponent},
  { path: 'admin/studentlist', component: AdminStudentTableComponent},
  { path: 'student/update', component: UpdateStudentComponent },
  { path: 'student/enrolledlist', component: StudentEnrolledListComponent },
  { path: 'student/viewlist', component: ViewActivityListComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(systemRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class SystemRoutingModule {
}
