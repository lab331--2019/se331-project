import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl,FormBuilder, Validators} from '@angular/forms'
import { ReactiveFormsModule } from '@angular/forms';
import { StudentService } from 'src/app/service/student-service';
import {MatDatepickerModule} from '@angular/material/datepicker';


import Student from '../../entity/student';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
  students: Student[];
  constructor(private fb:FormBuilder,private router:Router,private studentService: StudentService) {}
  form = this.fb.group({
    id: [''],
    studentId: ['',Validators.compose([Validators.required])],
    name: ['',Validators.compose([Validators.required])],
    surname: ['',Validators.compose([Validators.required])],
    dob: [''],
    image: [''],
    email: ['',Validators.compose([Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
    password: ['',Validators.compose([Validators.required])],
    confirmPassword: ['',Validators.compose([Validators.required])]
  },{
    validator: this.matchPassword('password', 'confirmPassword')
})

  validation_messages = {
    'studentId': [
      { type: 'required', message: 'Student Id is required'},
    ],
    'name': [
      {type: 'required', message: 'Please enter your name'}
    ],
    'surname': [
      { type: 'required', message: 'Please enter your surname'}
    ],
    'image': [],
    'birth': [
      { type: 'required', message: 'Please enter your birthday'},
      { type: 'pattern', message: 'Birthday should be in dd/mm/yyyy'}
    ],
    'email': [
      { type: 'required', message: 'Please enter your email'},
      { type: 'pattern', message: 'Email not in a correct format'}
    ],
    'password': [
      { type: 'required', message: 'Please enter your password'}
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'mustMatch', message: 'Passwords do not match'}
    ]
  };

  //Default image
  imgsrc: string = 'assets/images/person-icon.png';

  minDate: Date =  new Date(1900,0,1);
  maxDate: Date =  new Date();
  //Preview the image link
  previewImage() {
    this.imgsrc = this.form.get('image').value;
  }

  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }
  student: Student[] = [{
    'id': 1,
    'studentId': 'Stu-01',
    'name': 'dora',
    'surname': 'ameng',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571085893850&di=c5f6ede20f7a86c1eddd556cec214b8a&imgtype=0&src=http%3A%2F%2Fwww.36588.com.cn%3A8080%2FImageResourceMongo%2FUploadedFile%2Fdimension%2Fbig%2Faf9e5093-7f21-40f7-b4a8-cc4a73c6fe9e.png',
    'dob': new Date(1990,3,24),
    'email': 'jontron@cmu.ac.th',
    'password': '12345',
    'approved': true
  }, {
    'id': 2,
    'studentId': 'Stu-02',
    'name': 'mmonkey',
    'surname': 'lufy',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571680899&di=3330a14f611e3473ce9de1f4940756b1&imgtype=jpg&er=1&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201602%2F15%2F20160215231800_zrCN8.jpeg',
    'dob': new Date(1999,3,4),
    'email': 'pepsiamn@cmu.ac.th',
    'password': '54321',
    'approved': true
  }];
  duplicateId: boolean = false;
  

  submit() {
    const duplicateStudentId: Student = this.student.find(student => student.studentId === this.form.get('studentId').value );
    const duplicateEmail: Student = this.student.find(student => student.email === this.form.get('email').value );
    if ( (duplicateEmail || duplicateStudentId) != null) {
      if (duplicateStudentId!= null) alert("This studentId has been registered!");
      if (duplicateEmail!= null) alert("This email has been registered!");
    }
    else {
      this.studentService.saveStudent(this.form.value).subscribe(
        (student) => {
          console.log(this.form.value);
          console.log(this.studentService.getStudents());
          alert('Registration completed! please wait for validation from Admin.');
         
          this.router.navigate(['../login']);
         
        }, (error)=> { alert('There was an error registering your information, please try later!');
      })
    }
  }

 

  ngOnInit() {
  }

}
