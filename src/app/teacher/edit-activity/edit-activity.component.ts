import { Component, OnInit } from '@angular/core';
import { Params, Route, Router, ActivatedRoute } from '@angular/router';
import { ActivityService } from 'src/app/service/activity-service';
import Activity from 'src/app/entity/activity';
import { TeacherService } from 'src/app/service/teacher-service.service';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-edit-activity',
  templateUrl: './edit-activity.component.html',
  styleUrls: ['./edit-activity.component.css']
})
export class EditActivityComponent implements OnInit {
  constructor(private fb:FormBuilder,private route: ActivatedRoute,private router: Router, private activityService: ActivityService, private teacherService: TeacherService){}
  form = this.fb.group({
    //id: [''],
    name: ['',Validators.compose([Validators.required])],
    location: ['',Validators.compose([Validators.required])],
    description: ['',Validators.compose([Validators.required])],
    regisPeriodStart: [''],
    regisPeriodEnd: [''],
    activityPeriodStart: [''],
    activityPeriodEnd: [''],
    /*
    regisPeriodStartDate: [''],
    regisPeriodStartTime: [''],
    regisPeriodEndDate: [''],
    regisPeriodEndTime: [''],
    activityPeriodStart: [''],
    activityPeriodStartTime: [''],
    activityPeriodEndDate: [''],
    activityPeriodEndTime: [''],
    */
    //hostTeacher: ['']
    
  })

  validation_messages = {
    'name': [
      {type: 'required', message: 'Please enter the activity name'}
    ],
    'location': [
      { type: 'required', message: 'Please enter the activity location'}
    ],
    'description': [
      { type: 'required', message: 'Please enter the activity description'}
    ],
   
  };

  


  ngOnInit(): void {
    this.route.params
     .subscribe((params: Params) => {
     this.activityService.getActivity(+params['id'])
     .subscribe((inputActivity: Activity) => 
     {
       this.activity = inputActivity;
       this.form.get('name').patchValue(this.activity.name);
       this.form.get('location').patchValue(this.activity.location);
       this.form.get('description').patchValue(this.activity.description);
       this.form.get('regisPeriodStart').patchValue(this.activity.regisPeriodStart);
       this.form.get('regisPeriodEnd').patchValue(this.activity.regisPeriodEnd);
       this.form.get('activityPeriodStart').patchValue(this.activity.activityPeriodStart);
       this.form.get('activityPeriodEnd').patchValue(this.activity.activityPeriodEnd);
     }
     );
    
  })
}
exit() { this.router.navigate(['teacher/activitylist']); }

submit() {
  this.activity.name = this.form.get('name').value;
  this.activity.location = this.form.get('location').value;
  this.activity.description = this.form.get('description').value;
  this.activity.regisPeriodStart = this.form.get('regisPeriodStart').value;
  this.activity.regisPeriodEnd = this.form.get('regisPeriodEnd').value;
  this.activity.activityPeriodStart = this.form.get('activityPeriodStart').value;
  this.activity.activityPeriodEnd = this.form.get('activityPeriodEnd').value;
  console.log(this.activity);
  console.log(this.form.get('regisPeriodStart'));
  this.activityService.saveActivity(this.form.value).subscribe(
    (activity) => {
      alert('This activity has been updated!');
      
      this.router.navigate(['teacher/activitylist']);
    }, (error)=> { alert('could not save value');
  })
}
convertToDate(date: any) {
  if (date instanceof Date )
  return date.toISOString().split('T')[0];
  else return date;
}
  activity: Activity;
  
 
}
