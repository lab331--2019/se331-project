import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { StudentactivityDataSource } from './studentactivity-datasource';
import Student from 'src/app/entity/student';
import { ActivityService } from 'src/app/service/activity-service';
import { BehaviorSubject, Observable } from 'rxjs';
import { StudentService } from 'src/app/service/student-service';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import Activity from 'src/app/entity/activity';

@Component({
  selector: 'app-studentactivity',
  templateUrl: './studentactivity.component.html',
  styleUrls: ['./studentactivity.component.css']
})
export class StudentactivityComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatTable, {static: false}) table: MatTable<Student>;
  dataSource: StudentactivityDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['id', 'studentId', 'name', 'surname','email','approve', 'reject'];
  students: Student[];
  /*
  enrolledStudentIds = [1]; //Mock up
  waitingStudentIds = [2]; //Mock up
  */
  enrolledStudentIds : number[];
  waitingStudentIds : number[];
  fetchedStudents: Observable<Student[]>;

  activity: Activity;
    filter: string;
    filter$: BehaviorSubject<string>;
    constructor(
      private studentService: StudentService,
      private activityService: ActivityService,
      private route: ActivatedRoute,
      private router: Router) { }
  isEnrolled(id: number) {
    return this.enrolledStudentIds.includes(id);
  }
      addData() {
        
        this.studentService.getStudents()
        .subscribe(students => {
          setTimeout(
            () => {
                this.enrolledStudentIds = this.activity.enrolledStudents;
                this.waitingStudentIds = this.activity.waitingStudents;
                var allStudentsInAct = 
                this.enrolledStudentIds.concat(this.waitingStudentIds);
                //this.enrolledStudentIds.concat(this.waitingStudentIds);
                console.log(allStudentsInAct);
                students =  students.filter(element => allStudentsInAct.includes(element.id));
          this.dataSource = new StudentactivityDataSource();
          this.dataSource.data = students;
          this.dataSource.sort = this.sort;
          this.dataSource.paginator = this.paginator;
          this.filter$ = new BehaviorSubject<string>('');
          this.dataSource.filter$ = this.filter$;
          this.table.dataSource = this.dataSource;
          this.students = students;
          
        },0)}
        )
      }

      ngOnInit(): void {
        this.route.params
         .subscribe((params: Params) => {
         this.activityService.getActivity(+params['id'])
         .subscribe((inputActivity: Activity) => 
         {
           this.activity = inputActivity;
         }
         );
        
      })
    }
  
    ngAfterViewInit() {
      this.addData();
    }
    applyFilter(filterValue: string) {
      this.filter$.next(filterValue.trim().toLowerCase());
    }
    
    
    
  
    //Accept newly registered student
    approve(student:Student) {
     //this.enrolledStudentIds.push(student.id);
     this.enrolledStudentIds.push(student.id);
     this.waitingStudentIds = this.waitingStudentIds.filter( (value) => value != student.id);
     this.activity.enrolledStudents = this.enrolledStudentIds;
    
     //this.activity.enrolledStudents.push(student.id);
     this.activityService.saveActivity(this.activity);
     this.addData();
      //Save to DB
    }
    //Reject newly registered student
    reject(student:Student) {
      this.waitingStudentIds = this.waitingStudentIds.filter( (value) => value != student.id);
      this.activity.waitingStudents = this.waitingStudentIds;
      this.activityService.saveActivity(this.activity);
      this.addData();
      //Delete from DB
      //Refresh page
    }
    //Go back to activity list page
    backtoActivityListPage(){
      this.router.navigate(['teacher/activitylist']);
    }
  
  
  }
