import { Injectable } from '@angular/core';
import Teacher from '../entity/teacher';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class TeacherService {

  abstract getTeachers(): Observable<Teacher[]>;
  abstract getTeacher(id: number): Observable<Teacher>;
  abstract getTeacherByEmailAndPassword(email: string, password: string): Observable<Teacher>;
}
