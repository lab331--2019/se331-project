import { Injectable } from '@angular/core';
import  Student  from '../entity/student';
import { StudentService } from './student-service';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class StudentServiceImpl extends StudentService {
  //Get a specific student by studentId
  getStudentByStudentId(studentId: string): Observable<Student> {
    const output: Student = this.student.find(student => student.studentId === studentId);
    return of(this.student[output.id - 1 ]);
  }
  //Get a specific student by id
  getStudent(id: number): Observable<Student> {
    //const output: Student = this.student.find(student => student.id == id);
    return of(this.student[id - 1 ]);
  }
   //Get a specific student by email
  getStudentByEmail(email: string): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email);
    return of(this.student[output.id - 1 ]);
  }
  //Get all students in the DB
  getStudents(): Observable<Student[]> {
    return of(this.student);
  }
  //Update or save students
  saveStudent(student: Student): Observable<Student> {
    var output : Student = this.student.find(stu => stu.id === student.id)
    //If not in the DB yet, push
    if (output == null) {
      student.id =  this.student.length + 1;
     // student.id =  this.student.length + 1;
      this.student.push(student);
      console.log("added")
    } //Else, just replace
    else {
      console.log("replaced")
      
      this.student[student.id-1] = student;
    }
    return of(student);
  }

  getEnrollStudents(): Observable<Student[]> {
    throw new Error("Method not implemented.");
  }
  saveEnrollStudent(enrollStudent: Student): Observable<Student> {
    throw new Error("Method not implemented.");
  }
  //Get a specific student by email and password
  getStudentByEmailAndPassword(email: string, password: string): Observable<Student> {
    const output: Student = this.student.find(student => student.email === email && student.password === password);
    return of(this.student[output.id - 1 ]);
  }
  //Delete a specific student
  deleteStudent(id: number): Observable<Student> {
     var output: Student = null;
    for (var i =0; i < this.student.length; i++)
   if (this.student[i].id === id) {
     output = this.student[i];
    this.student.splice(i,1);
      break;
   }
   return of(output);
  }
  student: Student[] = [{
    'id': 1,
    'studentId': 'Stu-01',
    'name': 'dora',
    'surname': 'ameng',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571085893850&di=c5f6ede20f7a86c1eddd556cec214b8a&imgtype=0&src=http%3A%2F%2Fwww.36588.com.cn%3A8080%2FImageResourceMongo%2FUploadedFile%2Fdimension%2Fbig%2Faf9e5093-7f21-40f7-b4a8-cc4a73c6fe9e.png',
    'dob': new Date(1990,3,24),
    'email': 'dora@cmu.ac.th',
    'password': 'ameng',
    'approved': true
  }, {
    'id': 2,
    'studentId': 'Stu-02',
    'name': 'monkey',
    'surname': 'lufy',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571680899&di=3330a14f611e3473ce9de1f4940756b1&imgtype=jpg&er=1&src=http%3A%2F%2Fb-ssl.duitang.com%2Fuploads%2Fitem%2F201602%2F15%2F20160215231800_zrCN8.jpeg',
    'dob': new Date(1999,3,4),
    'email': 'lufy@cmu.ac.th',
    'password': 'monkey',
    'approved': true
  },{
    'id': 3,
    'studentId': 'Stu-03',
    'name': 'ace',
    'surname': '',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571680971&di=a37101ab4a764924b67a65623891c7ff&imgtype=jpg&er=1&src=http%3A%2F%2Fpic58.nipic.com%2Ffile%2F20150116%2F12299514_000824271000_2.jpg',
    'dob': new Date(1999,3,4),
    'email': 'ace@cmu.ac.th',
    'password': 'ace',
    'approved': false //Wait to be approved or else he can't login, if rejected then game over.
  },{
    'id': 4,
    'studentId': 'Stu-04',
    'name': 'auto',
    'surname': 'man',
    'image': 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1571086342830&di=1571ae8b354287c7f77c157aef686869&imgtype=0&src=http%3A%2F%2Fatt3.citysbs.com%2Fm320x%2Fhangzhou%2Fsns01%2Fforum%2F2011%2F01%2F18-18%2F20110118_2b0f68e19a7570661181wnsz0y8PjB3o.jpg',
    'dob': new Date(1998,5,4),
    'email': 'automan@cmu.ac.th',
    'password': 'automan',
    'approved': true
  }
  
];
  constructor() {
    super();
  }
}
