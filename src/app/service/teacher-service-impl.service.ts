import { Injectable } from '@angular/core';
import Teacher from '../entity/teacher';
import { TeacherService } from './teacher-service.service';
import { Observable, of } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TeacherServiceImpl extends TeacherService {
  
  teachers: Teacher[] = [
    {
    'id': 1,
    'name': 'Huang',
    'surname': 'Siyang',
    'email': 'hsy1349772335@gmail.com',
    'password': '1'
    },
    {
      'id': 2,
      'name': 'LI',
      'surname': 'Dongze',
      'email': '954748742@qq.com',
      'password': '2'
    }

  ]
  //Get all teachers in the DB
  getTeachers(): Observable<Teacher[]> {
    return of(this.teachers);
  }
  //Get a specific teacher by id
  getTeacher(id: number): Observable<Teacher> {
    const output: Teacher = this.teachers.find(teacher => teacher.id === id);
    return of(this.teachers[output.id - 1 ]);
  }
  //Get a specific teacher by email and password
  getTeacherByEmailAndPassword(email: string, password: string): Observable<Teacher> {
    const output: Teacher = this.teachers.find(teacher => teacher.email === email && teacher.password === password);
    return of(this.teachers[output.id - 1 ]);
  }

  constructor() {
    super();
  }
}
