import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-update-modal',
  templateUrl: './student-update-modal.component.html',
  styleUrls: ['./student-update-modal.component.css']
})
export class StudentUpdateModalComponent implements OnInit {
  ngOnInit(): void {
    throw new Error("Method not implemented.");
  }
  constructor(
    public dialogRef: MatDialogRef<StudentUpdateModalComponent>, private router: Router
    ) {}

  dismiss(): void {
    this.dialogRef.close();
    this.router.navigate(['student/viewlist']);
  }

}
