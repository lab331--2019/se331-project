import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewActivityListComponent } from './view-activity-list.component';

describe('ViewActivityListComponent', () => {
  let component: ViewActivityListComponent;
  let fixture: ComponentFixture<ViewActivityListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewActivityListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewActivityListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
