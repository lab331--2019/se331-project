import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { BehaviorSubject } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import { ViewActivityListDatasource } from './view-activity-list-datasource';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Teacher from 'src/app/entity/teacher';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-view-activity-list',
  templateUrl: './view-activity-list.component.html',
  styleUrls: ['./view-activity-list.component.css']
})
export class ViewActivityListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: ViewActivityListDatasource;

  displayedColumns = ['id', 'name', 'location', 'description', 'hostTeacher', 'regisPeriodStart', 'regisPeriodEnd', 'activityPeriodStart', 'activityPeriodEnd', 'enroll']
  acivities: Activity[];
  filter: string;
  filter$: BehaviorSubject<any>;
  activity: Activity;

  clicked: boolean;


  constructor(private activityService: ActivityService, private teacherService: TeacherService) { }
  
  ngOnInit() {
    this.activityService.getActivityList()
    .subscribe(activities => {
      this.dataSource = new ViewActivityListDatasource();
      this.dataSource.data = activities;
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
      this.filter$ = new BehaviorSubject<string>('');
      this.dataSource.filter$ = this.filter$;
      this.table.dataSource = this.dataSource;
      this.acivities = activities;
    })
  }

  // Date Filter 
  getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;

    console.log(fromDate, toDate);
   
  }

  pipe: DatePipe;

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });

 


  ngAfterViewInit() {
    this.addData();
  }
  addData() {
    throw new Error("Method not implemented.");
  }


  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  convertDate(date: string) { return eval(date); }

  getHostTeacher(hostTeacherId: number): string {
    var teacherN: string;
    this.teacherService.getTeacher(hostTeacherId).subscribe(
      (teacher) => {
        teacherN = teacher.name + " " + teacher.surname;
        console.log(teacherN);

      }
    )
    return teacherN;
  }

  clickAction() {
    if (confirm("Are you sure?")) {
      this.clicked = true
      console.log("Enrolled");
    }

  }

}
