import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { StudentService } from 'src/app/service/student-service';
import Student from 'src/app/entity/student';
import { MatDialog } from '@angular/material';
import { StudentUpdateModalComponent } from '../../student-update-modal/student-update-modal.component';

@Component({
  selector: 'app-update-student',
  templateUrl: './update-student.component.html',
  styleUrls: ['./update-student.component.css']
})
export class UpdateStudentComponent implements OnInit {

  constructor(public dialog: MatDialog,private route: ActivatedRoute, private fb:FormBuilder, private router:Router, private studentService: StudentService) { }

  form = this.fb.group({
    //id: [''],
    studentId: [''],
    name: ['',Validators.compose([Validators.required])],
    surname: ['',Validators.compose([Validators.required])],
    image: [''],
    dob: [''],
    email: [''],
    password: [''],
    confirmPassword: ['',Validators.compose([Validators.required])],
    approved: ['']
  },{
    validator: this.matchPassword('password', 'confirmPassword')
})

  validation_messages = {
    
    'name': [
      {type: 'required', message: 'Please enter your name'}
    ],
    'surname': [
      { type: 'required', message: 'Please enter your surname'}
    ],
    
    'birth': [
      { type: 'required', message: 'Please enter your birthday'},
      { type: 'pattern', message: 'Birthday should be in dd/mm/yyyy'}
    ],
    'email': [
      { type: 'required', message: 'Please enter your email'},
      { type: 'pattern', message: 'Email not in a correct format'}
    ],
    'password': [
      { type: 'required', message: 'Please enter your password'}
    ],
    'confirmPassword': [
      { type: 'required', message: 'Please enter your password'},
      { type: 'mustMatch', message: 'Passwords do not match'}
    ]
  };
  
  //Default image
  imgsrc: string = 'assets/images/person-icon.png';
    //Preview the image link
    previewImage() {
      this.imgsrc = this.form.get('image').value;
    }

  matchPassword(password:string,confirmPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[password];
      const matchingControl = formGroup.controls[confirmPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
          // return if another validator has already found an error on the matchingControl
          return;
      }
      // set error on matchingControl if validation fails
      matchingControl.setErrors( control.value!==matchingControl.value? {mustMatch: true} : null);
    }
  }

  student : Student;

 

  openDialog(): void {
    const dialogRef = this.dialog.open(StudentUpdateModalComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.router.navigate(['list']);
    });
  }

  submit() {
    this.openDialog();
  }

  ngOnInit():void {
    this.route.params
    .subscribe((params: Params) => {
      this.studentService.getStudent(4)
      .subscribe((inputStudent: Student) => this.student = inputStudent)
    });
    this.form.get('studentId').patchValue(this.student.studentId);
    this.form.get('name').patchValue(this.student.name);
    this.form.get('surname').patchValue(this.student.surname);
    this.form.get('email').patchValue(this.student.email);
    this.form.get('dob').patchValue(this.student.dob);
    this.form.get('password').patchValue(this.student.password);
    this.form.get('image').patchValue(this.student.image);
    this.previewImage();
  }

}
