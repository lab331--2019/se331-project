import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentEnrolledListComponent } from './student-enrolled-list.component';

describe('StudentEnrolledListComponent', () => {
  let component: StudentEnrolledListComponent;
  let fixture: ComponentFixture<StudentEnrolledListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentEnrolledListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentEnrolledListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
