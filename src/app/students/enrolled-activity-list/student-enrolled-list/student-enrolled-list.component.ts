import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTable } from '@angular/material';
import Activity from 'src/app/entity/activity';
import { StudentEnrolledListDatasource } from 'src/app/students/enrolled-activity-list/student-enrolled-list/student-enrolled-list-datasource';
import { BehaviorSubject, Observable } from 'rxjs';
import { ActivityService } from 'src/app/service/activity-service';
import Teacher from 'src/app/entity/teacher';
import { TeacherService } from 'src/app/service/teacher-service.service';
import Student from 'src/app/entity/student';
import { StudentService } from 'src/app/service/student-service';
import { DatePipe } from '@angular/common';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-student-enrolled-list',
  templateUrl: './student-enrolled-list.component.html',
  styleUrls: ['./student-enrolled-list.component.css']
})
export class StudentEnrolledListComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<Activity>;
  dataSource: StudentEnrolledListDatasource;
  
  displayedColumns = ['id', 'name', 'location', 'description', 'hostTeacher', 'regisPeriodStart', 'regisPeriodEnd', 'activityPeriodStart', 'activityPeriodEnd', 'pending']
  acivities: Activity[];
  students: Student[];
  enrolledStudentIds: number[];
  waitingStudentIds: number[];
  fetchedStudents: Observable<Student[]>;
  activity: Activity;

  filter: string;
  filter$: BehaviorSubject<any>;
  constructor(private activityService: ActivityService, private teacherService: TeacherService, private studentService: StudentService, ) { }
  hostTeachers: Teacher[]
  ngOnInit() {
    //Add teachers
    this.teacherService.getTeachers().subscribe(
      (teachers) => {
        this.hostTeachers = teachers;
      }
    )

  }

  // Date Filter 
  getDateRange(value) {
    // getting date from calendar
    const fromDate = value.fromDate;
    const toDate = value.toDate;

    console.log(fromDate, toDate);
    this.applyDateFilter(fromDate, toDate);
  }

  pipe: DatePipe;

  filterForm = new FormGroup({
    fromDate: new FormControl(),
    toDate: new FormControl(),
  });
  get fromDate() { return this.filterForm.get('fromDate'); }
  get toDate() { return this.filterForm.get('toDate'); }

  alertM(value:any) {
    console.log(this.getDateRange(value));
  }

  applyDateFilter(startDate : Date, endDate: Date) {
    
    //No date
    console.log(startDate,endDate);
    if(startDate == ( null ) && endDate == ( null )) { 
      this.applyFilter(''); 
    }
    //Only start date
    else if(startDate == ( null ) && endDate != ( null )) {
      this.filter$.next([0,endDate]);
    }
    //Only end date
    else if(startDate != ( null ) && endDate == ( null )) {
      this.filter$.next([startDate,Infinity]);
    }
    //Both
    else {
      this.filter$.next([startDate,endDate]);
    }
  }

  ngAfterViewInit() {
    this.addData();
  }

  addData() {
    this.activityService.getActivityList()
      .subscribe(activities => {
        this.dataSource = new StudentEnrolledListDatasource();
        this.dataSource.data = activities;
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.filter$ = new BehaviorSubject<string>('');
        this.dataSource.filter$ = this.filter$;
        this.table.dataSource = this.dataSource;
        this.acivities = activities;

      })
  }

  applyFilter(filterValue: string) {
    this.filter$.next(filterValue.trim().toLowerCase());
  }

  convertDate(date: string) { return eval(date); }

  getHostTeacher(hostTeacherId: number): string {
    var teacherN: string;
    this.teacherService.getTeacher(hostTeacherId).subscribe(
      (teacher) => {
        teacherN = teacher.name + " " + teacher.surname;
        console.log(teacherN);

      }
    )
    return teacherN;
  }

  isEnrolled(id: number) {
    return this.enrolledStudentIds.includes(id);
  }

}
