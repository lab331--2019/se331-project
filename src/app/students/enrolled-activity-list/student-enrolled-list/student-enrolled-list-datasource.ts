import Activity from 'src/app/entity/activity';
import { DataSource } from '@angular/cdk/table';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject, Observable, of as observableOf, merge } from 'rxjs';
import { map } from 'rxjs/operators';
import Student from 'src/app/entity/student';

export interface ActivityItem {
    id: number;
    name: string;
    location: string;
    description: string;
    hostTeacher: number; //Refer to teacher by id
    regisPeriodStart: Date;
    regisPeriodEnd: Date
    activityPeriodStart: Date;
    activityPeriodEnd: Date;
    
}
const EXAMPLE_DATA: Activity[] = [];
const EXAMPLE_DATAStu: Student[] = [];


export class StudentEnrolledListDatasource extends DataSource<Activity> {
    data: Activity[] = EXAMPLE_DATA;
    dataStu: Student[] = EXAMPLE_DATAStu;
    paginator: MatPaginator;
    sort: MatSort;
    filter$: BehaviorSubject<string>;
    constructor() {
        super();
    }

    connect(): Observable<Activity[]> {
        const dataMutations = [
            observableOf(this.data),
            this.paginator.page,
            this.sort.sortChange,
            this.filter$.asObservable()
        ];
        this.paginator.length = this.data.length;
        return merge(...dataMutations).pipe(map(() => {
            return this.getFilter(this.getPagedData(this.getSortedData([...this.data])));
        }));
    }

    disconnect() { }

    private getSortedData(data: Activity[]) {
        if (!this.sort.active || this.sort.direction === '') {
            return data;
        }

        return data.sort((a, b) => {
            const isAsc = this.sort.direction === 'asc';
            switch (this.sort.active) {
                case 'name': return compare(a.name, b.name, isAsc);
                case 'id': return compare(+a.id, +b.id, isAsc);
                default: return 0;
            }
        })
    }

    private getPagedData(data: Activity[]) {
        const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
        return data.splice(startIndex, this.paginator.pageSize);
    }

    private getFilter(data: Activity[]): Activity[] {
        const filter = this.filter$.getValue();
        if (filter === '') {
            return data;
        }
        //Date filter
        if (Array.isArray(filter)) {

            return data.filter((activity) => {

                var regisStart = activity.regisPeriodStart.getTime();
                var regisEnd = activity.regisPeriodEnd.getTime()
                return (regisStart >= filter[0] && regisEnd <= filter[1])
            })
        }
        return data.filter((activity) => {
            return (activity.name.toLowerCase().includes(filter));
        });
    }
}

function compare(a, b, isAsc) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}